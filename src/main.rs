use clap::Parser;
use itertools::Itertools;
use petgraph::{
    dot::{Config, Dot},
    matrix_graph::{NodeIndex, UnMatrix},
};
use std::{
    collections::{HashMap, HashSet},
    fs,
    io::prelude::*,
    process::{Command, Stdio},
};
use tempfile::{tempdir, TempDir};

/// Teams partitioning
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// The total number of players
    #[clap(value_parser)]
    players: usize,

    /// The number of rounds
    #[clap(value_parser)]
    rounds: usize,
}

fn main() {
    // TODO read player name list

    let args = Args::parse();

    let mut ctx = Context::new(args.players);

    if ctx.compute(args.rounds) {
        ctx.show();
    } else {
        eprintln!("Cannot compute! 🤖");
    }
}

struct Context {
    teams: HashMap<usize, Vec<(usize, usize)>>,
    nodes: Box<[NodeIndex]>,
    group: Vec<NodeIndex>,
    intern: UnMatrix<(), ()>,
    output: UnMatrix<(), ()>,
    directory: TempDir,
}

impl Context {
    fn new(players: usize) -> Self {
        assert!(players >= 8);

        let directory = tempdir().unwrap();

        // Generate team size combinations
        let max_4 = players / 4;
        let max_3 = players / 3;
        let teams = (0..=max_4)
            .cartesian_product(0..=max_3)
            .map(|(n, m)| (n * 4 + m * 3, (n, m)))
            .filter(|(p, _)| *p >= 8)
            .into_group_map();
        eprintln!(
            "Combinations={:?} ({}/{})",
            teams.values().map(|v| v.len()).sum::<usize>(),
            max_4,
            max_3
        );

        // The internal graph to keep track of player interactions
        let mut intern = UnMatrix::with_capacity(players);

        let group = Vec::new();

        let nodes = (0..players)
            .map(|_| intern.add_node(()))
            .collect_vec()
            .into_boxed_slice();

        // The graph(s) the user is interested in
        let output = intern.clone();

        // Fill graph with all possible edges
        for (a, b) in nodes.iter().copied().tuple_combinations() {
            intern.add_edge(a, b, ());
        }

        Self {
            teams,
            nodes,
            group,
            intern,
            output,
            directory,
        }
    }

    fn compute(&mut self, rounds: usize) -> bool {
        let players = self.nodes.len();
        let teams = self.teams[&players].clone();

        // Set of unused nodes
        let mut unused = HashSet::new();

        for sets in teams.iter().copied().combinations_with_replacement(rounds) {
            for (round, (n, m)) in sets.into_iter().enumerate() {
                debug_assert_eq!(players, n * 4 + m * 3);
                eprintln!("> Round {}, teams ({}, {})", round, n, m);

                self.reset_output();

                if self.compute_one(&mut unused, n, m) {
                    self.dump(round);
                } else {
                    eprintln!("Retry with next set");
                    self.reset_intern();
                    break;
                }
                if round == rounds - 1 {
                    return true;
                }
            }
        }

        eprintln!("No more sets");
        false
    }

    fn compute_one(&mut self, unused: &mut HashSet<NodeIndex>, n: usize, m: usize) -> bool {
        assert_eq!(self.output.edge_count(), 0);

        // Start with all nodes
        unused.clear();
        unused.extend(self.nodes.iter().copied());

        // Pick `n` teams of 4
        for _ in 0..n {
            if !self.find_group(unused, 4) {
                return false;
            }
        }

        // Pick `m` teams of 3
        for _ in 0..m {
            if !self.find_group(unused, 3) {
                return false;
            }
        }

        true
    }

    fn reset_intern(&mut self) {
        for (a, b) in self.nodes.iter().copied().tuple_combinations() {
            self.intern.update_edge(a, b, ());
        }
    }

    fn reset_output(&mut self) {
        for (a, b) in self.nodes.iter().copied().tuple_combinations() {
            if self.output.has_edge(a, b) {
                self.output.remove_edge(a, b);
            }
        }
    }

    // Try to find a new group of players who have not played together.
    fn find_group(&mut self, unused: &mut HashSet<NodeIndex>, size: usize) -> bool {
        // Try to find a node which has `size - 1` neighbors
        // to make a group of size `size`.

        // For each unused nodes (in this round)
        for node in unused.iter().copied() {
            self.group.clear();

            // Find compatible neighbors
            for neighbor in self.intern.neighbors(node) {
                // Select neighbor only if unused and not already connected to other neighbors
                if unused.contains(&neighbor)
                    && self
                        .group
                        .iter()
                        .all(|node| self.intern.has_edge(*node, neighbor))
                {
                    self.group.push(neighbor);
                    if self.group.len() == size - 1 {
                        // We have enough neighbors
                        break;
                    }
                }
            }

            if self.group.len() == size - 1 {
                self.group.push(node);
                break;
            }
        }

        if self.group.len() != size {
            return false;
        }

        // Remove group nodes from unused (making them used)
        for node in self.group.iter() {
            unused.remove(node);
        }

        for (a, b) in self.group.iter().copied().tuple_combinations() {
            self.intern.remove_edge(a, b);
            self.output.update_edge(a, b, ());
        }

        true
    }

    fn dump(&self, round: usize) {
        let process = Command::new("dot")
            .arg("-Tpng")
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()
            .unwrap();

        let data = format!(
            "{:?}",
            Dot::with_config(&self.output, &[Config::NodeIndexLabel, Config::EdgeNoLabel])
        );

        process
            .stdin
            .as_ref()
            .unwrap()
            .write_all(data.as_bytes())
            .unwrap();

        let output = process.wait_with_output().unwrap();

        fs::write(
            self.directory.path().join(format!("g{}", round)),
            &output.stdout,
        )
        .unwrap();
    }

    fn show(&self) {
        let mut process = Command::new("feh")
            .arg(self.directory.path())
            .spawn()
            .unwrap();
        process.wait().unwrap();
    }
}
